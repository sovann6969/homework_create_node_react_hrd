import React, { Component } from "react";
import Item from "./Item";


class Items extends Component {
    constructor(props) {
        super(props);
        this.state = {
            changeText: '',
            items: [],
        };
    }

    handleChange(event) {
        this.setState({ changeText: event.target.value })
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        var arr =[]
        arr = this.state.items
        arr.push({id:this.state.items.length,
                value:this.state.changeText})
        this.setState({ array:arr,changeText:'' })
    }

    handleDelete = itemId => {
        const items = this.state.items.filter(item => item.id !== itemId);
        this.setState({ items: items });
    };

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" value={this.state.changeText}
                        onChange={this.handleChange.bind(this)} />
                        <button type="submit" className="btn-success btn btn-sm text-white ml-4">Submit</button>
                </form>
            
                {this.state.items.map(item => (
                    <Item 
                        key={item.id}
                        onClick={this.handleDelete}
                        item={item}
                    />
                ))}
                <p>{this.props.changeText}</p>
            </div>
        );
    }
}

export default Items;