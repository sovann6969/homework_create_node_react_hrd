import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
class Item extends Component {
 
  render() {
    const mystyle = {
      color: "white",
      borderStyle: "solid",
       borderWidth: "10px ",
      borderColor: '#d6d7da',
      padding: "10px",
      fontFamily: "Arial",
      width:"200px",
      height:"150px",
      display:"inline-block",
      margin:"5px"
    };
 
    return (
      <div  style={mystyle} >
        <div>
            <button
              onClick={() => this.props.onClick(this.props.item.id)}
              className="btn btn-lg btn-warning text-white" >
              Delete
            </button>
            <p className="text-danger">{this.props.item.value}</p>
        </div>
      </div>
    ); 
  }
 
}
 
export default Item;